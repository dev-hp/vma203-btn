// OLED SSD1306: I2C, SDA=A4, SCL=A5
#include <U8x8lib.h>
#include <Wire.h>
U8X8_SSD1306_128X64_NONAME_HW_I2C u8x8(/* reset=*/ U8X8_PIN_NONE);

int last_adc_in = -1;

void setup() {
  // put your setup code here, to run once:
  // OLED setup
  u8x8.begin();
  u8x8.setFont(u8x8_font_amstrad_cpc_extended_r);
  u8x8.clear();
  u8x8.setContrast(64);
  u8x8.draw1x2String(0, 0, "VMA203 button");
}

void loop() {
  // put your main code here, to run repeatedly:
  int adc_key_in = analogRead(0);
  if (adc_key_in != last_adc_in) {
    u8x8.setCursor(0, 3);
    u8x8.print(adc_key_in);
    u8x8.print("     ");
    last_adc_in = adc_key_in;
  }
}
